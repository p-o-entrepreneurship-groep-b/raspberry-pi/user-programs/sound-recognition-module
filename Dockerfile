FROM debian:stretch-slim

RUN apt-get update && apt-get -y dist-upgrade && \
    apt-get -y install --no-install-recommends build-essential libssl-dev libffi-dev libblas3 libc6 liblapack3 gcc \
                                               cmake libz-dev libtinfo-dev \
                                               python3.5 python3-dev python3-pip cython3 python3-numpy python3-sklearn python3-h5py && \
    rm -rf /var/lib/apt/lists/*
RUN printf "[global]\nextra-index-url=https://www.piwheels.org/simple" >> /etc/pip.conf && pip3 install --upgrade setuptools
WORKDIR /template
COPY template/requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

WORKDIR /home/arne/test-librosa
COPY test-librosa ./
RUN cmake --build . --target install

WORKDIR /code
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
WORKDIR /template
COPY template .
WORKDIR /code
COPY [^t]* ./
CMD ["python3", "continous_mic_recog.py"]
