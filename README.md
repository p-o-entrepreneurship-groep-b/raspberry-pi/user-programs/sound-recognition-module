## audio-recognition-module

In order to function properly, the model expects a /test-librosa directory, with the built source code of LLVM (version 7.0.1 or, probably, others). This is because a certain version of the librosa library is needed, which requires a specific version of the llvmlite library for Python. Not only does this version not have wheels for the ARM architecture used by the Pi, the installation through pip is actually broken. After a lot of searching we managed to successfully install llvmlite by first manually compiling LLVM. In order not to commit the entire LLVM source code into this repository, it is put inside the .gitignore, and will need to be done manually when using the module.

